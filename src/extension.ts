import * as vscode from "vscode";
import { PackageManager } from "./dependencies";
import { Output } from "./output";

export function activate(context: vscode.ExtensionContext) {
  Output.getInstance().show();

  let disposables = [
    vscode.commands.registerCommand(
      "bundlephobia-support.getPackageInfo",
      PackageManager.getPackageInfo
    ),
    vscode.commands.registerCommand(
      "bundlephobia-support.scanPackageJson",
      PackageManager.scanPackageJson
    ),
  ];

  context.subscriptions.push(...disposables);
}

export function deactivate() {}
