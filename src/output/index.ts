import * as vscode from "vscode";
import { sizeToKBs } from "../dependencies/helpers";

export class Output {
  private static instance: Output;
  private constructor(
    private channel: vscode.OutputChannel = vscode.window.createOutputChannel(
      "Bundlephobia"
    )
  ) {}

  public static getInstance(): Output {
    if (!Output.instance) {
      Output.instance = new Output();
    }

    return Output.instance;
  }

  write(value: string) {
    this.channel.append(value);
  }

  log(value: string) {
    this.channel.appendLine(value);
  }

  error(value: string) {
    this.channel.appendLine(`\x1b[31m${value}\x1b[0m`);
  }

  clear() {
    this.channel.clear();
  }

  show() {
    this.channel.show();
  }

  logPackageSizes(pack: { name: any; size: number; gzip: number }) {
    if (!pack) {
      return;
    }
    this.log(
      `${pack.name}: minified: ${sizeToKBs(
        pack.size
      )} KBs, minified+gzipped: ${sizeToKBs(pack.gzip)} KBs`
    );
  }

  logSummaryInformation(
    depsMinifiedSizes: number,
    depsGzippedSizes: number,
    countOfPackages: number
  ) {
    this.log(
      `\nTotal packages: ${countOfPackages}\nOverall minified: ~${sizeToKBs(
        depsMinifiedSizes
      )} KBs\nOverall minified+gzipped: ~${sizeToKBs(
        depsGzippedSizes
      )} KBs\n(sizes without custom dependancies)\n___________________________________\n`
    );
  }
}
