import * as vscode from "vscode";
import * as fs from "fs";
import { BundlePhobiaClient } from "../http/BundlePhobiaClient";
import { Output } from "../output";
import {
  getPackagesInfoFromPackageJson,
  scanFoldersForPackageJsons,
  sizeToKBs,
  summarizeGzippedSizes,
  summarizeSizes,
} from "./helpers";

export class PackageManager {
  static async scanPackageJson() {
    let outputChannel = Output.getInstance();
    outputChannel.clear();

    for await (const packageJson of await scanFoldersForPackageJsons()) {
      if (!packageJson.name) {
        outputChannel.log(
          `Please provide a "name" to your project in package.json file placed inside ${packageJson.projectFolder}`
        );
        packageJson.name = packageJson.projectFolder;
      }
      outputChannel.log(
        `Start to look at ${packageJson.name} package.json file`
      );

      let calcedPackages = await getPackagesInfoFromPackageJson(packageJson);

      outputChannel.log(`Summary for ${packageJson.name}:`);
      calcedPackages.forEach(outputChannel.logPackageSizes, outputChannel);

      let depsMinifiedSizes = calcedPackages.reduce(summarizeSizes, 0);
      let depsGzippedSizes = calcedPackages.reduce(summarizeGzippedSizes, 0);
      outputChannel.logSummaryInformation(
        depsMinifiedSizes,
        depsGzippedSizes,
        calcedPackages.length
      );
    }
  }
  static async getPackageInfo() {
    let packageName = await vscode.window.showInputBox({
      placeHolder: "package @packages/package version@1.2.3",
      prompt: "Enter package name(s) space separated with or without version.",
      ignoreFocusOut: true,
    });
    let packageInfo = await BundlePhobiaClient.getPackageInfo(packageName);
    let buttons = ["github", "bundlephobia"];
    let action = await vscode.window.showInformationMessage(
      `Size of ${packageName} is: ${sizeToKBs(
        packageInfo.size
      )}KBs minified; + Gzipped: ${sizeToKBs(packageInfo.gzip)}KBs`,
      ...buttons
    );
    switch (action) {
      case "github":
        vscode.commands.executeCommand(
          "vscode.open",
          vscode.Uri.parse(packageInfo.repository)
        );
        break;
      case "bundlephobia":
        vscode.commands.executeCommand(
          "vscode.open",
          vscode.Uri.parse(`https://bundlephobia.com/result?p=${packageName}`)
        );
        break;

      default:
        break;
    }
  }

  static getPackageJsonDepsFromEditor() {
    let activeEditor = vscode.window.activeTextEditor;
    if (
      !activeEditor ||
      !activeEditor?.document.fileName.endsWith("package.json")
    )
      return;

    let packageJson = JSON.parse(activeEditor.document.getText());

    return Object.keys(packageJson?.dependencies ?? {}).map(
      (dep) => dep + "@" + packageJson?.dependencies[dep]
    );
  }
}
