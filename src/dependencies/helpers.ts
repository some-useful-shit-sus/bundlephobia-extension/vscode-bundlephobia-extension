import { BundlePhobiaClient } from "../http/BundlePhobiaClient";
import { workspace } from "vscode";
import { readFileSync } from "fs";

/**
 * TODO: make DTO model for packageInfo object
 * @returns array of packageInfo objects
 */
export async function getPackagesInfoFromPackageJson(packageJson: {
  dependencies: { [x: string]: string };
}) {
  const deps = Object.keys(packageJson?.dependencies ?? {}).map(
    (dep) => dep + "@" + packageJson?.dependencies[dep]
  );

  let calcedPackages = await Promise.all(
    deps.map((pack) => BundlePhobiaClient.getPackageInfo(pack))
  );
  return calcedPackages.filter((e) => e);
}
/**
 * TODO: make DTO model for packageJson
 * @returns packageJson object
 */
export async function scanFoldersForPackageJsons() {
  let files = await workspace.findFiles("package.json", "**​/node_modules/**");
  return files.map((file) => {
    let packageJson = JSON.parse(readFileSync(file.path, "UTF-8"));
    packageJson.projectFolder = file.path.split("/").slice(0, -1).join("/");
    return packageJson;
  });
}
export function sizeToKBs(size: number): string {
  return (size / 1024).toFixed(1);
}

export function summarizeSizes(acc: number, pack: { size: number }): number {
  return (acc += pack.size);
}

export function summarizeGzippedSizes(
  acc: number,
  pack: { gzip: number }
): number {
  return (acc += pack.gzip);
}
