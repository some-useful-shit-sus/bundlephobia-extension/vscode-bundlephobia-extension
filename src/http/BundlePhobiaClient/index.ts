import got from "got";

export class BundlePhobiaClient {
  static async get(uri: string) {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    const headers = { Accept: "application/json" };
    try {
      const response = await got.get(uri, {
        prefixUrl: "https://bundlephobia.com/api/",
        headers,
      });
      return JSON.parse(response.body);
    } catch (error) {
      if (error.response) {
        console.error(error.response.body);
      } else {
        console.error(error);
      }
    }
  }

  static async getPackageInfo(packageString: string | undefined) {
    return await BundlePhobiaClient.get(
      `size?package=${packageString}&record=true`
    );
  }

  static async getExports(packageString: string) {
    return await BundlePhobiaClient.get(`exports?package=${packageString}`);
  }

  static async getExportsSizes(packageString: string) {
    return await BundlePhobiaClient.get(
      `exports-sizes?package=${packageString}`
    );
  }

  static async getHistory(packageString: string) {
    return await BundlePhobiaClient.get(
      `package-history?package=${packageString}`
    );
  }

  static async getRecentSearches(limit: string) {
    return await BundlePhobiaClient.get(`recent?limit=${limit}`);
  }

  static async getSimilar(packageName: string) {
    return await BundlePhobiaClient.get(
      `similar-packages?package=${packageName}`
    );
  }
}
