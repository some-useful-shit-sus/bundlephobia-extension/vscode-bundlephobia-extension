# BundlePhobia support for vscode

Adds support of [bundlephobia](https://bundlephobia.com/) package sizes investigation via command palette

-------------------------------------------------

## Features
 - Taking sizes info about specific package ![Taking info about specific package](images/1.gif)
 - Taking sizes info from package.json files scanning all the workspace folders ![Taking info overall info from package.json](images/2.gif)

## Release Notes

### 1.0.0

Initial release of bundlephobia support extension.

-----------------------------------------------------------------------------------------------------------
